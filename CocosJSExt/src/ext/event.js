/**
 * @author zhouzhanglin
 * 事件
 */
var ext = ext || {}

/**
 * @class Event 事件基类 
 */
var __Event = cc.Class.extend({
	_type:'',//事件类型
	_userdata:null, //数据
	_target:null,  //抛出事件的对象
	_isDefaultPrevented:false,//是否阻止默认的事件侦听回调方法
	_isStopPropagation:false,//是否阻止子节点执行此事件
	
	/**
	 * 构造函数.
	 */
	ctor:function(eventType){
		this._type=eventType;
	},
	/**
	 * 阻止事件流
	 */
	stopPropagation: function()
	{
		this._isStopPropagation = true;
	},
	preventDefault: function()
	{
		this._isDefaultPrevented = false;
	},
	getUserData:function(){
		return this._userdata;
	},
	setUserData:function(value){
		this._userdata = value;
	},
	getType:function(){
		return this._type;
	},
	getCurrentTarget:function(){
		return this._target;
	}
});
__Event.COMPLETE ="complete";
__Event.INIT ="init";
__Event.CHANGE ="change";
__Event.OPEN ="open";
__Event.CLOSE = "close";
__Event.ENTER_FRAME="enterFrame";
__Event.SELECT="select";
cc.defineGetterSetter(__Event.prototype, "userdata", __Event.prototype.getUserData,  __Event.prototype.setUserData);
cc.defineGetterSetter(__Event.prototype, "type", __Event.prototype.getType, undefined);
cc.defineGetterSetter(__Event.prototype, "target", __Event.prototype.getCurrentTarget, undefined);
ext.Event=__Event;

//============================================================
/**
 * @class touch事件
 */
var __TouchEvent = __Event.extend({
	_touch:null,
	_event:null,
	ctor:function(type,touch,event){
		this._super(type);
		this._type = type;
		this._touch = touch;
		this._event = event;
	},
	getTouch:function(){
		return this._touch;
	},
	getEvent:function(){
		return this._event;
	}
});
__TouchEvent.BEGAN = "began";
__TouchEvent.MOVED = "moved";
__TouchEvent.ENDED = "ended";
__TouchEvent.CANCELED = "canceled";
__TouchEvent.CLICK = "CLICK";
__TouchEvent.REAL_CLICK = "REAL_CLICK"; //点击移动小于6像素才触发
cc.defineGetterSetter(__TouchEvent.prototype, "touch", __TouchEvent.prototype.getTouch, undefined);
cc.defineGetterSetter(__TouchEvent.prototype, "event", __TouchEvent.prototype.getEvent, undefined);
ext.TouchEvent = __TouchEvent;

//============================================================

/**
 * @class EventDispatcher 事件分发器
 */
var __EventDispatcher = cc.Class.extend({
	_eventArray : null, //对当前类的侦听 事件
	/**
	 * 构造函数
	 */
	ctor:function()
	{
		this._eventArray={};
	},
	
	/**
	 * 添加事件侦听
	 * @param eventType {string} 事件类型
	 * @param listener {function} 方法
	 */
	addEventListener: function(eventType,listener)
	{
		if(this._eventArray.hasOwnProperty(eventType))
		{
			this._eventArray[eventType].push(listener);
		}
		else
		{
			this._eventArray[eventType]=[listener];
		}
	},
	/**
	 * 移除事件侦听
	 * @param eventType {string} 侦听类型
	 * @param listener {function} 侦听方法，为null时删除所有的eventType侦听
	 */
	removeEventListener: function(eventType,listener)
	{
		if(this._eventArray.hasOwnProperty(eventType))
		{
			if(!listener) delete this._eventArray[eventType];
			return;
			
			var listeners = this._eventArray[eventType];
			for(var i=0;i<listeners.length ; i++)
			{
				var lis = listeners[i];
				if(lis==listener)
				{
					listeners.splice(i,1);
					i--;
					if(listeners.length==0)
					{
						delete this._eventArray[eventType];
						break;
					}
				}
			}
		}
	},
	/**
	 * 判断是否有相应的事件侦听
	 * @param eventType {string} 事件类型
	 * @param listener {function} 侦听方法，可为空 
	 * @returns {Boolean} 
	 */
	hasEventListener:function(eventType,listener)
	{
		if(this._eventArray.hasOwnProperty(eventType))
		{
			if(listener==null) return true;

			var listeners = this._eventArray[eventType];
			for(var i=0;i<listeners.length ; i++)
			{
				var lis = listeners[i];
				if(lis==listener)
				{
					return true;
				}
			}
		}
		return false;
	},
	/**
	 * 抛出事件
	 * @param event {ext.Event} 事件
	 */
	dispatchEvent: function(event)
	{
		if(this._eventArray.hasOwnProperty(event.getType()))
		{
			event._target= this;
			var listeners = this._eventArray[event.getType()];
			for(var i = 0 ;i<listeners.length;i++)
			{
				var lis = listeners[i];
				lis(event);
			}
		}
	},
	/**
	 * 移除所有的侦听 
	 */
	removeAllListeners:function()
	{
		this._eventArray = {}
	},
	dispose:function()
	{
		this._eventArray = null;
	}

});
ext.EventDispatcher = __EventDispatcher;

